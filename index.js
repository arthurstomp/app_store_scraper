#!/bin/sh
':' //; exec "$(command -v nodejs || command -v node)" "$0" "$@"

var fs = require('fs');
var store = require('app-store-scraper');
var { Parser } = require('json2csv');

const fields = ['id', 'userName', 'date', 'score', 'title', 'text']
const opts = { fields }

function outputResult(res) {
  var processRes = getImportantStuff(res)
  var csv = toCsv(processRes)
  const filePath = process.argv[3]
  if(filePath) {
    var stream = fs.createWriteStream(filePath)
    csv.forEach(function(el) {
      stream.write(el)
    })
    console.log('Review were save on ',filePath)
  } else {
    csv.forEach(function(el) {
      console.log(el)
    })
  }
}

function getImportantStuff(json) {
  return json.map(function(review) {
    return {
      id: review['id'],
      userName: review['userName'],
      date: review['date'],
      score: review['score'],
      title: review['title'],
      text: review['text']
    }
  })
}

function toCsv(data) {
  try {
    const parser = new Parser(opts)
    csv = data.map(function(el) {
      return parser.parse(el)
    })
    return csv
  } catch(err) {
    console.error(err);
  }
}

function scrapPage(page) {
  console.log('fetching page',page)
  return store.reviews({
    id: process.argv[2],
    page: page,
    sort: store.sort.RATING
  })
}

// I will run until it receives an empty array as result.
async function scrapAllPages() {
  var pageToFetch = 0;
  var resultBuff = [];
  var res = await scrapPage(pageToFetch);
  //console.log('res length', res.length)
  while (pageToFetch < 10) {
    resultBuff = resultBuff.concat(res)
    //console.log('resultBuff length', resultBuff.length);
    pageToFetch++;
    res = await scrapPage(pageToFetch);
    //console.log('res length', res.length)
  }
  console.log('Total of reviews scraped :',resultBuff.length);
  outputResult(resultBuff);
}

scrapAllPages()
