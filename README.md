# Little Google Play review scrapper

## Usage

```
$ ./index.js <id-to-scrape> <filename-for-output>
```

## Where is the app id

![Where the id is](./where_id_is.png?raw=true)
